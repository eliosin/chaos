# Quickstart icon

- [icon Prerequisites](/eliosin/icon/prerequisites.html)
- [Installing icon](/eliosin/icon/installing.html)

## Creating `ico` format file from a `png`

```
icotool -c favicon.ico -o favicon.ico
```

## Guide

![](/eliosin/icon/elio72pt.png)

72pt elio star with black background and a white halo.

![](/eliosin/icon/elio24pt.png)

24pt elio star with black background and a white halo. Useful for smaller icon

![](/eliosin/icon/elio12pt.png)

12pt elio star with black background. Useful for smaller favicons
